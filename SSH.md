### ¿Qué es SSH?
Para conectarnos desde otro PC de nuestro compañero y para que podamos ver el gparted ejecutándose en el otro ordenador. Eso de podernos connectar es a través de las X11 fordwaring.
Las X11 lo que hacen es dibujar lo que nosotros queremos que nos muestre por pantalla. Lo que hacen las X11 es dibujar y quien se encarga de ejecutar son la *CPU*, la *RAM* y el *Disco Duro*.

Y todo esto lleva a la conclusión que para podernos conectar desde nuestro PC al de nuestro compañero/a hay que seguir una serie de pasos.
* systemctl status sshd
* systemctl start sshd
* ssh -X root@192.168.4.4

